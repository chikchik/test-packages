﻿namespace Pathfinding.Core
{
    /// <summary>
    /// Класс приоритетной очереди, реализована в виде двунаправленного связного списка
    /// </summary>
    public class PriorityQueue<T>
    {
        private PathNode<T> head;

        public bool Empty
        {
            get { return head == null; }
        }

        public void Push(PathNode<T> item)
        {
            item.Next = null;
            item.Prev = null;

            if (head == null)
            {
                head = item;
            }
            else
            {
                PathNode<T> iter = head;
                PathNode<T> last = head;
                while (iter != null)
                {
                    last = iter;
                    if (item.Cost < iter.Cost)
                        break;

                    iter = iter.Next;
                }

                if (iter == null)
                {
                    last.Next = item;
                    item.Prev = last;
                    item.Next = null;
                }
                else if (last.Prev == null)
                {
                    head = item;
                    item.Prev = null;
                    item.Next = last;
                    last.Prev = item;
                }
                else
                {
                    item.Prev = last.Prev;
                    item.Next = last;
                    last.Prev.Next = item;
                    last.Prev = item;
                }
            }
        }

        public PathNode<T> Pop()
        {
            if (Empty)
                return null;

            PathNode<T> item = head;
            if (head.Next != null)
            {
                head = head.Next;
                head.Prev = null;
            }
            else
            {
                head = null;
            }

            item.Next = null;

            return item;
        }

        public void Clear()
        {
            head = null;
        }

        public void Remove(PathNode<T> item)
        {
            if (item.Prev != null)
                item.Prev.Next = item.Next;
            else
                head = item.Next;

            if (item.Next != null)
                item.Next.Prev = item.Prev;
        }

        public void Update(PathNode<T> item)
        {
            PathNode<T> iter = item;
            if (iter.Prev != null && item.Cost < iter.Prev.Cost)
            {
                while (iter != null)
                {
                    if (item.Cost < iter.Prev.Cost)
                    {
                        iter = iter.Prev;
                        if (iter.Prev == null)
                            break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else if (iter.Next != null && item.Cost > iter.Next.Cost)
            {
                while (iter != null)
                {
                    if (item.Cost > iter.Next.Cost)
                    {
                        iter = iter.Next;
                        if (iter.Next == null)
                            break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                return;
            }

            if (iter == null || item.Equals(iter))
                return;

            Remove(item);

            if (iter.Prev == null)
            {
                head = item;
                item.Prev = null;
                item.Next = iter;
                iter.Prev = item;
            }
            else if (iter.Next == null)
            {
                iter.Next = item;
                item.Prev = iter;
                item.Next = null;
            }
            else
            {
                item.Prev = iter.Prev;
                item.Next = iter;
                iter.Prev.Next = item;
                iter.Prev = item;
            }
        }

        private bool CheckConsistent()
        {
            PathNode<T> iter = head;
            while (iter != null)
            {
                var prev = iter;
                iter = iter.Next;

                if (iter == null)
                    break;

                if (iter.Prev != prev)
                {
                    return false;
                }
            }

            return true;
        }
    }
}