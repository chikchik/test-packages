﻿using System;
using System.Collections.Generic;

namespace Pathfinding.Core
{
    /// <summary>
    /// Основной класс для вычисления и построения пути аглоритмом A*
    /// </summary>
    public class PathFinder<T>
    {
        private readonly ISurroundProvider<T> surroundProvider_;

        private readonly PriorityQueue<T> open_ = new PriorityQueue<T>();
        private static readonly Dictionary<int, PathNode<T>> pathNodes_ = new Dictionary<int, PathNode<T>>(700);
        private static readonly FastPool<PathNode<T>> pathNodesPool_ = new FastPool<PathNode<T>>(700);
        private IAlgorithm<T> algorithm_;
        private PathNode<T> currentPathNode_;
        private readonly Action<T> surroundIterator_;

        private int recalcNodesCount_;
        private int calcNodesCount_;

        /// <summary>
        /// Днина пути
        /// </summary>
        public float PathLength
        {
            get { return currentPathNode_ != null ? currentPathNode_.Distance : 0f; }
        }

        public T BestNode
        {
            get { return currentPathNode_ != null ? currentPathNode_.WorldNode : default(T); }
        }

        public PathFinder(ISurroundProvider<T> surroundProvider)
        {
            surroundProvider_ = surroundProvider;
            surroundIterator_ = IterateSurround;
        }

        public static void ClearCache()
        {
            pathNodes_.Clear();
            pathNodesPool_.Clear();
        }

        /// <summary>
        /// Вычисление пути без его построения
        /// </summary>
        public void Calculate(T from, IAlgorithm<T> algorithm)
        {
            recalcNodesCount_ = 0;
            calcNodesCount_ = 0;

            open_.Clear();
            pathNodes_.Clear();
            pathNodesPool_.Reset();
            algorithm_ = algorithm;

            PathNode<T> startPathNode = pathNodesPool_.Get().Init(from, null);
            startPathNode.Status = PathNodeStatus.Open;
            pathNodes_[from.GetHashCode()] = startPathNode;
            open_.Push(startPathNode);

            while (open_.Empty == false)
            {
                currentPathNode_ = open_.Pop();

                if (algorithm_.IsDone(currentPathNode_))
                {
                    pathNodes_.Clear();
                    //Debug.Log("PathFInder (found): recalcNodesCount: {0}  calcNodesCount: {1}", recalcNodesCount_, calcNodesCount_);
                    return;
                }

                currentPathNode_.Status = PathNodeStatus.Close;

                surroundProvider_.IterateSurround(currentPathNode_.WorldNode, surroundIterator_);
            }

            pathNodes_.Clear();
            // точный путь не найден, но выбирается лучший узел
            currentPathNode_ = algorithm.BestNode;

            //Debug.Log("PathFInder (not found): recalcNodesCount: {0}  calcNodesCount: {1}", recalcNodesCount_, calcNodesCount_);
        }

        public Path<T> CalculateAndConstructPath(T from, IAlgorithm<T> algorithm, List<T> nodes = null)
        {
            Calculate(from, algorithm);
            return ConstructPath(nodes);
        }

        /// <summary>
        /// Построение пути
        /// </summary>
        private Path<T> ConstructPath(List<T> nodes = null)
        {
            if (currentPathNode_ == null)
                return null;

            PathNode<T> endNode = currentPathNode_;
            nodes = nodes ?? new List<T>();
            nodes.Clear();
            nodes.Add(endNode.WorldNode);

            while (endNode.Parent != null)
            {
                endNode = endNode.Parent;
                nodes.Insert(0, endNode.WorldNode);
            }

            return new Path<T>(nodes, currentPathNode_.Distance);
        }

        /// <summary>
        /// Построение пути
        /// </summary>
        public Path<T> ConstructPath(PathNode<T> pathNode, List<T> nodes = null)
        {
            PathNode<T> endNode = pathNode;
            nodes = nodes ?? new List<T>();
            nodes.Clear();
            nodes.Add(endNode.WorldNode);

            while (endNode.Parent != null)
            {
                endNode = endNode.Parent;
                nodes.Insert(0, endNode.WorldNode);
            }

            return new Path<T>(nodes, pathNode.Distance);
        }

        /// <summary>
        /// Итератор ближайшего узла
        /// </summary>
        /// <param name="surroundNode"></param>
        private void IterateSurround(T surroundNode)
        {
            PathNode<T> pathNode;
            if (pathNodes_.TryGetValue(surroundNode.GetHashCode(), out pathNode) == false)
            {
                pathNode = pathNodesPool_.Get().Init(surroundNode, currentPathNode_);
                pathNodes_[surroundNode.GetHashCode()] = pathNode;
            }
            else if (pathNode.Status == PathNodeStatus.Invalid)
            {
                return;
            }

            float g = currentPathNode_.Distance + algorithm_.CalculateDistance(currentPathNode_, pathNode);
            if (pathNode.Status == PathNodeStatus.None)
            {
                calcNodesCount_++;
                pathNode.Distance = g;
                if (algorithm_.IsValid(pathNode))
                {
                    pathNode.Heuristic = algorithm_.CalculateHeuristic(pathNode.WorldNode);
                    algorithm_.ProcessNode(pathNode);
                    pathNode.Status = PathNodeStatus.Open;
                    open_.Push(pathNode);
                }
                else
                {
                    pathNode.Status = PathNodeStatus.Invalid;
                }
            }
            else
            {
                if (g < pathNode.Distance)
                {
                    recalcNodesCount_++;
                    pathNode.Distance = g;
                    pathNode.Parent = currentPathNode_;
                    algorithm_.ProcessNode(pathNode);

                    if (pathNode.Status == PathNodeStatus.Close)
                    {
                        pathNode.Status = PathNodeStatus.Open;
                        open_.Push(pathNode);
                    }
                    else
                    {
                        open_.Update(pathNode);
                    }
                }
            }
        }
    }
}