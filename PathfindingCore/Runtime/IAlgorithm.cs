﻿namespace Pathfinding.Core
{
    /// <summary>
    /// Интерфейс алгоритма для определения оптимального пути
    /// </summary>
    public interface IAlgorithm<T>
    {
        /// <summary>
        /// Лучший, из найденных, узел.
        /// Путь расчитывается от него, если точный путь не найден
        /// </summary>
        PathNode<T> BestNode { get; }

        /// <summary>
        /// Достигнут ли конечный узел
        /// </summary>
        bool IsDone(PathNode<T> pathNode);

        /// <summary>
        /// Подходит ли данный узел
        /// </summary>
        bool IsValid(PathNode<T> pathNode);

        /// <summary>
        /// Обработать данные узла пути
        /// </summary>
        void ProcessNode(PathNode<T> pathNode);

        /// <summary>
        /// Вычислить дистанцию между узлами
        /// </summary>
        float CalculateDistance(PathNode<T> from, PathNode<T> to);

        /// <summary>
        /// Вычислить значение эвристики
        /// </summary>
        float CalculateHeuristic(T from);
    }
}