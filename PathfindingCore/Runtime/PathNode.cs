﻿using System;

namespace Pathfinding.Core
{
    public enum PathNodeStatus
    {
        None,
        Open,
        Close,
        Invalid
    }

    /// <summary>
    /// Класс узла пути
    /// </summary>
    public sealed class PathNode<T> : IComparable<PathNode<T>>
    {
        public T WorldNode;
        public PathNode<T> Parent;
        public float Heuristic;
        public float Distance;
        public float Cost;
        public PathNodeStatus Status;
        public PathNode<T> Next;
        public PathNode<T> Prev;

        public PathNode()
        {
        }

        public PathNode(T worldNode, PathNode<T> parent)
        {
            WorldNode = worldNode;
            Parent = parent;
        }

        public int CompareTo(PathNode<T> obj)
        {
            PathNode<T> to = obj;
            if (to == null)
                return 0;
            if (Cost < to.Cost)
                return -1;
            if (Cost > to.Cost)
                return 1;
            return 0;
        }

        public PathNode<T> Init(T worldNode, PathNode<T> parent)
        {
            WorldNode = worldNode;
            Parent = parent;
            Status = PathNodeStatus.None;
            return this;
        }
    }
}