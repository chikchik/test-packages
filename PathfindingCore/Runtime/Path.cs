﻿using System.Collections.Generic;
using System.Text;

namespace Pathfinding.Core
{
    /// <summary>
    /// Класс построенного пути
    /// </summary>
    public class Path<T>
    {
        public IList<T> Nodes { get; private set; }
        public float Length { get; private set; }

        public Path(IList<T> nodes, float length)
        {
            Nodes = nodes;
            Length = length;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Nodes.Count; i++)
            {
                T worldNode = Nodes[i];
                sb.AppendLine(worldNode.ToString());
            }

            sb.AppendLine(string.Format("Path length = {0}", Length));
            return sb.ToString();
        }
    }
}