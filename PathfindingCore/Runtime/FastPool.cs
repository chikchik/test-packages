﻿using System.Collections.Generic;

namespace Pathfinding.Core
{
    public class FastPool<T> where T : new()
    {
        private readonly List<T> list_;
        private int seek_;

        public int Seek
        {
            get { return seek_; }
        }

        public FastPool(int capacity = 0)
        {
            list_ = new List<T>(capacity);
        }

        public T Get()
        {
            if (seek_ >= list_.Count)
            {
                T item = new T();
                list_.Add(item);
                seek_++;
                return item;
            }
            else
            {
                return list_[seek_++];
            }
        }

        public void Alloc(int totalCount)
        {
            int count = totalCount - list_.Count;
            for (int i = 0; i < count; i++)
            {
                T item = new T();
                list_.Add(item);
            }
        }

        public void Reset()
        {
            seek_ = 0;
        }

        public void Clear()
        {
            list_.Clear();
        }
    }
}