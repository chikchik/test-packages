﻿namespace Pathfinding.Core
{
    public enum HeuristicMethod
    {
        None,
        Euclidian,
        Manhattan
    }
}