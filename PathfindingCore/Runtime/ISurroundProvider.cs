﻿using System;

namespace Pathfinding.Core
{
    /// <summary>
    /// Интерфейсс предоставляющий информацию об окружащих узлах в мире
    /// </summary>
    public interface ISurroundProvider<T>
    {
        void IterateSurround(T node, Action<T> iterator);
    }
}